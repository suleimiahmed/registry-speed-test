# ubuntu:latest base image is negligible ~ 28.17 MB
FROM ubuntu:latest 
# use shred to generate a pseudo random file (its faster than other ways)
# https://www.gnu.org/software/coreutils/manual/html_node/Random-sources.html#Random-sources
RUN touch random_file && shred -n 1 -s 5G random_file