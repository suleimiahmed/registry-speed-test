# registry-speed-test

registry-speed-test pushes a random 5Gb layer (on top of the ubuntu base layer) to: GitLab container registry, gcr and ecr in order to compare push times. The push is conducted from both kaninko and docker clients to help identify if a particular client is responsible for slow pushes.

## Running the pipeline

To run the speed test, you can fork this pipeline, trigger the necessary jobs and calculate the time taken to push.

The pipelines are run selectively by a manual trigger of the associated jobs in the pipeline.

> NOTE: To run the gcr jobs successfully you must set the following CI variables:

- `GCP_SA_KEY` : A base 64 encoded GCP service account key with the necessary access to your gcr.
- `GCR_REGISTRY_PATH`: The container registry path to your gcr instance.

> NOTE: To run the ecr jobs successfully you must set the following CI variables:

- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`
-  `AWS_DEFAULT_REGION`
 
 These variables are defined [here](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html)

You also need to additionally set the following extra CI variables for ecr pushes:

- `AWS_REGISTRY` : The container registry path to ecr.
- `AWS_REGISTRY_REPO` : The ecr repository name to push to. This needs to be manually created in the ecr registry prior to push.
